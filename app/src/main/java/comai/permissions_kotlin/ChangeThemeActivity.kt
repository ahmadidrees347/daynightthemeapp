package comai.permissions_kotlin

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class ChangeThemeActivity : AppCompatActivity() {
    private lateinit var btnTheme: Button

    lateinit var editor: SharedPreferences.Editor
    private var selectedTheme: String = ""
    private lateinit var sharedPreferences: SharedPreferences
    var initialized = false

    private fun init() {
        if (!initialized) {
            initialized = true
            sharedPreferences = this.getSharedPreferences("pref", Context.MODE_PRIVATE)
        }
        editor = sharedPreferences.edit()
        if ("" == selectedTheme) {
            selectedTheme = sharedPreferences.getString("selected_theme", "AppTheme")!!
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (selectedTheme.equals("AppTheme")) {
            application.setTheme(R.style.AppTheme)
        } else {
            application.setTheme(R.style.AppThemeDark)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_theme)


        btnTheme = findViewById(R.id.btn_Theme)
        btnTheme.setOnClickListener {
            editor = sharedPreferences.edit()
            if (selectedTheme.equals("DarkTheme")) {
                editor.putString("selected_theme", "AppTheme")
            } else {
                editor.putString("selected_theme", "DarkTheme")
            }
            editor.apply()
            editor.commit()

            recreate()
        }
    }

    override fun getTheme(): Resources.Theme {
        init()
        val theme = super.getTheme()
        when (selectedTheme) {
            "DarkTheme" -> {
                theme.applyStyle(R.style.AppThemeDark, true)
            }
            "AppTheme" -> {
                theme.applyStyle(R.style.AppTheme, true)
            }
        }
        return theme
    }
}