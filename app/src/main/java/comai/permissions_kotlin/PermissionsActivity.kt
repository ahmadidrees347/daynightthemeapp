package comai.permissions_kotlin

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Resources
import android.net.wifi.WifiManager
import android.net.wifi.WifiNetworkSuggestion
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.core.app.ActivityCompat
import java.io.File
import java.io.FileOutputStream

class PermissionsActivity : AppCompatActivity() {

    var PERMISSION_ALL = 1
    var PERMISSIONS = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )


    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permissions)

        val btn_permission = findViewById(R.id.btn_permission) as Button
        btn_permission.setOnClickListener {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL)
        }

        if (!hasPermissions(this, *PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL)
        }



        /*val fBaseDir =
            File(getFilesDir().absolutePath, "my-app")
        if(!fBaseDir.exists()) {
            fBaseDir.mkdirs()
        }
        val gpxfile = File(fBaseDir, "my-app")
        val writer = FileWriter(gpxfile)
        writer.append("ABCD")
        writer.flush()
        writer.close()
        Toast.makeText(applicationContext, "Saved", Toast.LENGTH_SHORT).show()

        Log.e("Path", getFilesDir().absolutePath)
        val dirFiles: File = getFilesDir()
        for (strFile in dirFiles.list()) {
            // strFile is the file name
            Log.e("Path", strFile)
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            var builder = WifiNetworkSuggestion.Builder()
            builder.setSsid("YOUR_SSID")
                .setWpa2Passphrase("YOUR_PASSWORD")
            var suggestion = builder.build()

            var list = arrayListOf<WifiNetworkSuggestion>();
            list.add(suggestion);

            val manager =
                getApplicationContext().getSystemService(Context.WIFI_SERVICE) as WifiManager
            var status = manager.addNetworkSuggestions(list);

            if (status == WifiManager.STATUS_NETWORK_SUGGESTIONS_SUCCESS) {
                //We have successfully added our wifi for the system to consider
            }

            for (x in 0..list.size - 1) {
                Log.e("DATA", list.get(x).toString())
            }
        }



        try {
            val root: File = this!!.getExternalFilesDir(null)!!
            if (!root.exists()) {
                root.mkdirs()
            }
            val f = File(root.path + "/" + "mttext.txt")
            if (f.exists()) {
                f.delete()
            }
            f.createNewFile()
            val out = FileOutputStream(f)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Path", e.toString())
        }
        /*
        * Naviaget User to App Setting If permissions is disabled*/
//        val intent = Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS)
//        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//        startActivity(intent)
    }
    fun hasPermissions(context: Context, vararg permissions: String): Boolean = permissions.all {
        ActivityCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissionsList: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_ALL -> {
                if (grantResults.size > 0) {
                    var permissionsDenied = ""
                    for (per in permissionsList) {
                        Log.e("permissionsDenied", "bhb" + per)
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            Log.e("permissionsDenied", "" + permissionsDenied)
                            val showRationale =
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    shouldShowRequestPermissionRationale(per)
                                } else {
                                    TODO("VERSION.SDK_INT < M")
                                    Log.e("permissionsDenied", "never ask again")
                                    false
                                }
                            if (!showRationale) {
                                Log.e("permissionsDenied", "never ask again")
                                // user also CHECKED "never ask again"
                                // you can either enable some fall back,
                                // disable features of your app
                                // or open another dialog explaining
                                // again the permission and directing to
                                // the app setting
                            }
                        }
                    }
                }
                return
            }
        }
    }
}