package comai.permissions_kotlin;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class ChangeThemeActivityJ extends AppCompatActivity {

    SharedPreferences.Editor editor;
    private String selectedTheme = "";
    private SharedPreferences sharedPreferences;
    boolean initialized = false;

    private void init() {
        if (!initialized) {
            initialized = true;
            sharedPreferences = this.getSharedPreferences("pref", Context.MODE_PRIVATE);
        }
        editor = sharedPreferences.edit();
        if ("" == selectedTheme) {
            selectedTheme = sharedPreferences.getString("selected_theme", "AppTheme");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (selectedTheme.equals("AppTheme")) {
            getApplication().setTheme(R.style.AppTheme);
        } else {
            getApplication().setTheme(R.style.AppThemeDark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_theme);


        findViewById(R.id.btn_Theme).setOnClickListener(v -> {
            editor = sharedPreferences.edit();
            if (selectedTheme.equals("DarkTheme")) {
                editor.putString("selected_theme", "AppTheme");
            } else {
                editor.putString("selected_theme", "DarkTheme");
            }
            editor.apply();
            editor.commit();

            recreate();
        });
    }

    @Override
    public Resources.Theme getTheme() {
        init();
        Resources.Theme theme = super.getTheme();
        switch (selectedTheme) {
            case "DarkTheme": {
                theme.applyStyle(R.style.AppThemeDark, true);
            }
            case "AppTheme": {
                theme.applyStyle(R.style.AppTheme, true);
            }
        }
        return theme;
    }
}